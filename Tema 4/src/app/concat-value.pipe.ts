import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'concatValue'
})
export class ConcatValuePipe implements PipeTransform {

  transform(value: string, addValue: string): string {
    return value+addValue;
  }

}
