import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appColorText]'
})
export class ColorTextDirective {
  
  highlightColor:string="";

  constructor(private el: ElementRef) {}

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.highlightColor || 'green');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight("");
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
