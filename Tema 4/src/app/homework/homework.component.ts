import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-homework',
  templateUrl: './homework.component.html',
  styleUrls: ['./homework.component.scss']
})
export class HomeworkComponent implements OnInit {

  numar:number=15;
  title:string="un titlu banal";
  date:Date=new Date();
  backColor:string="Yellow";

  names:string[]=[
    "ana",
    "maria",
    "george",
    "florin",
    "andreea"
  ];

  dates:Date[]=[
    new Date("2015-10-10"),
    new Date("2018-11-18"),
    new Date("2020-05-06"),
    new Date("2011-10-30"),
    new Date("1999-01-01")
  ];

  word:string="Marea";
  constructor() { }

  ngOnInit(): void {
  }

}
