import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {  
  title = 'notes-app';
  titlu:string ="TITLU";
  data:Date=new Date(10,22,2021);
  myValue:number =5;
}
