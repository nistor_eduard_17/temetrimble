import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Category } from '../category';
import { HomeComponent } from '../home/home.component';
import { Note } from '../note';
import { NoteComponent } from '../note/note.component';
import { FilterService } from '../services/filter.service';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  currentCategory: string;
  currentColor:string;
  categories: Category[] = [];

  validateForm: FormGroup;

  description: FormControl;
  color: FormControl;
  categoryId: FormControl;
  title: FormControl;

  receivedTitle:string;
  receivedColor:string;
  receivedDescription:string;
  receivedCategoryId:string;

  constructor(private service: NoteService, private category: FilterService,private dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) data
    ) { 
      this.receivedTitle=data.title;
      this.receivedCategoryId=data.categoryId;
      this.receivedDescription=data.description;
      this.receivedColor=data.color;
    }

  ngOnInit(): void {

    this.description = new FormControl(' ', [Validators.required, Validators.minLength(2)]);
    this.color = new FormControl(' ', [Validators.required, , Validators.minLength(2)]);
    this.categoryId = new FormControl(' ', [Validators.required, Validators.pattern('1|2|3')]);
    this.title = new FormControl(' ', [Validators.required, Validators.minLength(2)]);

    this.validateForm = new FormGroup({
      'title': this.title,
      'description': this.description,
      'category': this.categoryId,
      'color': this.color

    });
    
    this.title.setValue(this.receivedTitle);
    this.description.setValue(this.receivedDescription);
    this.categoryId.setValue(this.receivedCategoryId);
    this.color.setValue(this.receivedColor);
    this.setHTMLColorAndCategory();
    
  }

  public setHTMLColorAndCategory(){

    this.currentColor=this.color.value;
    if (this.categoryId.value === "1")
      this.currentCategory = "To Do"
    else
      if (this.categoryId.value === "2")
        this.currentCategory = "Done"
      else
        if (this.categoryId.value === "3")
          this.currentCategory = "Doing"


  }

  public updateNote() {
    HomeComponent.rNote.title =this.title.value;
    HomeComponent.rNote.description = this.description.value;
    HomeComponent.rNote.color = this.color.value;
    HomeComponent.rNote.categoryId = this.categoryId.value;
    this.service.UpdateEditedNote(HomeComponent.rNote.id).subscribe();
  }

  public setCategory() {
    this.currentCategory="";
    this.categories = this.category.getFilters();

  }

  public modifyColor(){
    this.currentColor="";
  }

  save() {
    this.updateNote();
    this.dialogRef.close(this.validateForm.value);
}

close() {
    this.dialogRef.close();
}


}
