import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddNoteComponent } from './add-note/add-note.component';
import { EditComponent } from './edit/edit.component';
import { HomeComponent } from './home/home.component';
import { HomeworkComponent } from './homework/homework.component';

const routes: Routes = [
  { path: "", component: HomeComponent, pathMatch:"full" },
  { path: "add-note", component: AddNoteComponent},
  {path:"edit-note",component:EditComponent},
  {path: "homework/:id", component: HomeworkComponent},
  { path: '**', redirectTo: ''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
