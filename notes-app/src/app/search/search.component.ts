import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() emitSearchContent=new EventEmitter<string>();
  
  title:string= "Add note";
  titleColor:string ="red";
  noteContent:string= "";
  
  constructor() { }

  ngOnInit(): void {
  }

  // functie (){
  //   this.noteContent="Note content modificat din buton"
  // }
  // setTitle() { this.title = "test"; }

  onSearchClicked(){
    this.emitSearchContent.emit(this.noteContent);
  }

}
