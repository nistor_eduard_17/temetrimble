import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from '../category';
import { FilterComponent } from '../filter/filter.component';
import { Note } from '../note';
import { FilterService } from '../services/filter.service';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
  // template: 'Favorite Color: <input type="text" [formControl]="favoriteColorControl"'
})
export class AddNoteComponent implements OnInit {

  categories: Category[] = [];

  validateForm: FormGroup;
  description: FormControl;
  color: FormControl;
  categoryId: FormControl;
  title: FormControl;

  constructor(private service: NoteService, private category: FilterService){}

  ngOnInit(): void {

    this.description = new FormControl(' ', [Validators.required, Validators.minLength(2)]);
    this.color = new FormControl(' ', [Validators.required,, Validators.minLength(2)]);
    this.categoryId = new FormControl(' ',[Validators.required, Validators.pattern('1|2|3')]);
    this.title = new FormControl(' ', [Validators.required, Validators.minLength(2)]);

    this.validateForm=new FormGroup({
      'title':this.title,
      'description':this.description,
      'category':this.categoryId,
      'color':this.color

    });

  }

  public addNote() {
    const note:Note={
      title:this.title.value,
      description:this.description.value,
      color:this.color.value,
      categoryId:this.categoryId.value
    }
    this.service.addNote(note).subscribe();
  }

  public setCategory() {
    this.categories = this.category.getFilters();
  }

  public Submit(){
    this.service.getNotes().subscribe();
  }

}
