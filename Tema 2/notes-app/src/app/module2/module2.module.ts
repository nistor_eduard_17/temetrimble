import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component3Component } from './component3/component3.component';
import { Module3Module } from './module3/module3.module';



@NgModule({
  declarations: [
    Component3Component
  ],
  imports: [
    CommonModule,
    Module3Module
  ],
  exports: [
    Component3Component,
    Module3Module
  ]
})
export class Module2Module { }
