import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component5Component } from './component5/component5.component';
import { Module3Module } from '../module3.module';
import { Module2Module } from '../../module2.module';



@NgModule({
  declarations: [
    Component5Component
  ],
  imports: [
    CommonModule
  ],
  exports: [
    Component5Component
  ]
})
export class Module4Module { }
