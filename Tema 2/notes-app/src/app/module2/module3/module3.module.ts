import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component4Component } from './component4/component4.component';
import { Module4Module } from './module4/module4.module';



@NgModule({
  declarations: [
    Component4Component
  ],
  imports: [
    CommonModule,
    Module4Module
  ],
  exports: [
    Component4Component,
    Module4Module
  ]
})
export class Module3Module { }
